package com.luc.comp.ds;


public class Thread_Join {

	public static void main(String[] args) {
		
		Thread t1=new Thread(new Threadsample());
		Thread t2=new Thread(new Threadsample());
		t1.start();

		t2.start();
		try {
			t1.join();

			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("End of main method");

	}

}

class Threadsample extends Thread{
	
	static int tm=1000;
	public void run(){
		Thread th=Thread.currentThread();
		try {
			th.sleep(tm);
			tm=tm+1000;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Entered into thread");
	}
	
}

