package com.luc.comp.ds;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;


public class Task_Schedule {
	
		
	public HashMap BuiltDependencies(String filename) throws IOException{
		String line;
		String initialtask;
		HashMap predecessor_Map= new HashMap();
		HashMap successor_Map=new HashMap();
		HashMap pred_succ_map= new HashMap();
		FileReader fis=new FileReader(filename);
		BufferedReader br=new BufferedReader(fis);
		 while((line = br.readLine()) != null) {
			 
           String[] line_tokens=line.split(",");
           
           
           
           successor_Map.put(line_tokens[0], line_tokens[1]);
           if(line_tokens[1]=="0"){
        	   initialtask=line_tokens[0];         	   
           }
             }// end of while method
		
		Set tasks=successor_Map.keySet();
		Iterator task_iterator=tasks.iterator();
		
		while(task_iterator.hasNext()){
			String task_val=task_iterator.next().toString();
			String succ_val=successor_Map.get(task_val).toString(); // passing key and getting value 
			
			if(succ_val!="0"){
				String[] succ_tokens=succ_val.toString().split("\\.");
				
				for(int i=0;i<succ_tokens.length;i++){
					
					if(predecessor_Map.get(succ_tokens[i])== null){
						predecessor_Map.put(succ_tokens[i],task_val);
					}
					else{
						String prev_val=predecessor_Map.get(succ_tokens[i]).toString();
						predecessor_Map.put(succ_tokens[i],""+prev_val+"."+task_val);
					
					}
				}
				
			}//end of if to verify having no predecessors
		
			
		}// end of for loop for building predecessor map
		
		pred_succ_map.put("pred", predecessor_Map);
		pred_succ_map.put("succ",successor_Map);
		
		return pred_succ_map;
		
	}// end of BuiltDependencies method
	
	
	
	
	
	
/*This method is to get the list of successors that can be run*/	
	
	public Vector getSuccessors(String task){
		String success_task_list="";
		Vector<String> execute_tasks=new Vector<String>();
		if(Start_Here.predecessor_Map.containsKey(task)){
		success_task_list=Start_Here.predecessor_Map.get(task).toString();
		}
				
		String[] succes_task_tokens=success_task_list.split("\\.");
		for(int i=0;i<succes_task_tokens.length &&succes_task_tokens[i]!="" ;i++){
			if(isValidToExecute(succes_task_tokens[i])){
				execute_tasks.add(succes_task_tokens[i]);
			}
		}
		
		return execute_tasks;
		
	}



/*This method will check whether all predecessors were completed for given task*/


	private boolean isValidToExecute(String task) {
		
		boolean valid=true;
		String pred_task_list=Start_Here.successor_Map.get(task).toString();
		String[] pred_task_tokens=pred_task_list.split("\\.");
		for(int i=0;i<pred_task_tokens.length;i++){
			if(Start_Here.finished_tasks.indexOf(pred_task_tokens[i])==-1){
				valid=false;
				
			}
			
		}
		
		
		return valid;
	}  // end of isvalidtoexecute method
	
	
	
	public boolean check_cyclic(HashMap predecessor_Map,HashMap successor_Map){
		boolean is_cyclic=true;
		
		Vector<String> tasks=new Vector(successor_Map.keySet());
		Iterator tasks_iterator=tasks.iterator();
		
		for(int i=0;i<tasks.size();i++){
			String success_map_entries="";
			String task=tasks_iterator.next().toString();
			if(predecessor_Map.containsKey(task)){
			success_map_entries=successor_Map.get(task).toString();
			}
			String[] success_map_entry_tokens=success_map_entries.split("\\.");
			for(int j=0;j<success_map_entry_tokens.length;j++){
				String task_value=success_map_entry_tokens[j];
				if(task_value=="")
					continue;
				String pred_map_entries=predecessor_Map.get(task).toString();
				String[] pred_map_entries_tokens=pred_map_entries.split("\\.");
				for(int k=0;k<pred_map_entries_tokens.length;k++){
					if(task_value.equals(pred_map_entries_tokens[k])){
						is_cyclic=false;
					}
					
				}
				
				
			}//end of j for loop
			
		}//end of for loop
		
		
		return is_cyclic;
	}
	

}
