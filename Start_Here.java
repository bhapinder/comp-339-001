package com.luc.comp.ds;


import java.io.IOException;
import java.util.*;

import javax.print.attribute.standard.Finishings;

public class Start_Here {
	public static Vector<String> finished_tasks;
	public static Vector<String> inprogress_tasks;
	public static Vector<String> pending_tasks;
	public static Vector<String> Just_Completed;
	public static HashMap predecessor_Map;
	public static HashMap successor_Map;
	public static boolean main_alive;
	
	public static void main(String ar[]) throws IOException, InterruptedException{
		
		main_alive=true;
		HashMap pred_succ_map;
		finished_tasks=new Vector();
		inprogress_tasks=new Vector();
		Just_Completed=new Vector();
		
		
		Task_Schedule ts=new Task_Schedule();
		pred_succ_map=ts.BuiltDependencies("C:\\workspace\\DistributedSystems\\src\\com\\luc\\comp\\ds\\Dependency_Info.txt");
		predecessor_Map=(HashMap)pred_succ_map.get("pred");
		successor_Map=(HashMap)pred_succ_map.get("succ");
		pending_tasks=new Vector(successor_Map.keySet());
		boolean is_cyclic=ts.check_cyclic(predecessor_Map,successor_Map);
		if(!is_cyclic){
			System.out.println("You have cyclic tasks that never completes. Please check dependency configuration. Exitting gracefully");
			System.exit(0);
			
		}
		String initialtask_list=predecessor_Map.get("0").toString();
		String[] initial_task_tokens=initialtask_list.split("\\.");
		for(int i=0;i<initial_task_tokens.length;i++){
		Thread runTask=new Thread(new Run_Task(initial_task_tokens[i]));
		runTask.start();
		}
		int i=0;
		while(main_alive){
			if(Just_Completed.size()>=1){
				
				for(i=0;i<Just_Completed.size();i++){
					Vector successor_tasks=ts.getSuccessors(Just_Completed.elementAt(i));
					Just_Completed.remove(Just_Completed.elementAt(i));
					
					for(int j=0;j<successor_tasks.size();j++){
						if(successor_tasks.elementAt(j).toString()=="0"){
							main_alive=false;
						}
						if(!finished_tasks.contains(successor_tasks.elementAt(j).toString()) && !inprogress_tasks.contains(successor_tasks.elementAt(j).toString())){
						Thread th=new Thread(new Run_Task(successor_tasks.elementAt(j).toString()));
						th.start();
						inprogress_tasks.add(successor_tasks.elementAt(j).toString());
						}// start thread only if it doesn't finish
					}
				}
				
			}
			else{
				if(pending_tasks.size()==0)
				{
					System.out.println("All tasks are completed");
					main_alive=false;
				}
				Thread.currentThread().sleep(1000);
			}
			
		}// end of while loop finish of program
		
		
	}

}
