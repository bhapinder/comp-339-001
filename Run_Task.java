package com.luc.comp.ds;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.omg.CORBA.portable.ApplicationException;


public class Run_Task extends Thread {
	String task;
	String str;
	
	Run_Task(String task){
		this.task=task;
		this.str=str;
	}

	public void run(){

		Properties prop=new Properties();
		try {
			prop.load(new FileInputStream("C:\\workspace\\DistributedSystems\\src\\com\\luc\\comp\\ds\\Task_Description.txt"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		String taskname=prop.getProperty(task);
		if(taskname.equals("SUM1000")){
			int sum=0;
			for(int i=0;i<1000;i++){
				sum=sum+i;
				
			}
			System.out.println("Sum of 1000 number is "+sum);
			
		}
		if(taskname.equals("SLEEP10000")){
			try {
				System.out.println("Task "+task+" is going to sleep now");
				Thread.currentThread().sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("Task"+task+" is completed");
		Start_Here.finished_tasks.add(task);
		Start_Here.Just_Completed.add(task);
		Start_Here.inprogress_tasks.remove(task);
		Start_Here.pending_tasks.remove(task);
		
	
	}
	
}
